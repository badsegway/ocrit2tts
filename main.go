package main

/*
 * Script to screenshot + OCR webpages.
 * Useful for online readers
 * Requires (install via apt):
 *	- tesseract-ocr
 *  - xdotool
 *  - x11-utils
 *  - imagemagick
 */

import (
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func cmd(cmdlineInf interface{}) string {
	var cmd *exec.Cmd
	var cmdsplit []string
	if cmdline, ok := cmdlineInf.(string); ok {
		cmdsplit = strings.Fields(cmdline)
	} else if cmdline, ok := cmdlineInf.([]string); ok {
		cmdsplit = cmdline
	}
	cmd = exec.Command(cmdsplit[0], cmdsplit[1:]...)
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", stdoutStderr)
	return string(stdoutStderr)
}

func ocr(picPath, output string) {
	cmd([]string{"tesseract", picPath, output, "-l", "eng"})
}

func screenshot(bounds, output string) string {
	return cmd([]string{"import", "-window", "root", "-crop", bounds, output})
}

func moveToNextPage(page *int) {
	*page++
	cmd("xdotool key Right")
}

func getWindowBounds(windowTitle string) string {
	var bounds string
	var x, y, w, h int
	var topYOffset = 120
	var bottomYOffset = 40
	var leftXOffset = 80
	var rightXOffset = 80

	fmt.Println("[*] Please select the Kindle Cloud Reader window ...")
	// Grab specific location of selected window with crop; ie: 1211x1882+1988+84
	selectedWindowInfo := cmd("xwininfo")

	re := regexp.MustCompile(`Absolute.*X:\s+(\d+)`)
	matches := re.FindStringSubmatch(selectedWindowInfo)
	x, _ = strconv.Atoi(matches[1])

	re = regexp.MustCompile(`Absolute.*Y:\s+(\d+)`)
	matches = re.FindStringSubmatch(selectedWindowInfo)
	y, _ = strconv.Atoi(matches[1])

	re = regexp.MustCompile(`Width:\s+(\d+)`)
	matches = re.FindStringSubmatch(selectedWindowInfo)
	w, _ = strconv.Atoi(matches[1])

	re = regexp.MustCompile(`Height:\s+(\d+)`)
	matches = re.FindStringSubmatch(selectedWindowInfo)
	h, _ = strconv.Atoi(matches[1])

	// Remove 100 pixels from our bounds
	// (enough to get rid of our browser's "header" and "footer")
	y += topYOffset
	h -= (topYOffset + bottomYOffset)
	x += leftXOffset
	w -= (leftXOffset + rightXOffset)

	// WIDTH x HEIGHT + INITIAL X + INITIAL Y
	bounds = fmt.Sprintf("%dx%d+%d+%d", w, h, x, y)

	fmt.Printf("%+v\n", bounds)
	return bounds
}

func md5sum(filePath string) string {
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}

	return fmt.Sprintf("%x", h.Sum(nil))
}

func overMaxConcurrentPicDupes(picPath, lastScreenshotHash *string, concurrentHashDupes *int) bool {
	hash := md5sum(*picPath)
	if hash == *lastScreenshotHash {
		*concurrentHashDupes++
		if *concurrentHashDupes > 5 {
			return true
		}
	} else {
		*concurrentHashDupes = 0
		*lastScreenshotHash = hash
	}
	return false
}

// sortName returns a filename sort key with
// non-negative integer suffixes in numeric order.
// For example, amt, amt0, amt2, amt10, amt099, amt100, ...
func sortName(filename string) string {
	ext := filepath.Ext(filename)
	name := filename[:len(filename)-len(ext)]
	// split numeric suffix
	i := len(name) - 1
	for ; i >= 0; i-- {
		if '0' > name[i] || name[i] > '9' {
			break
		}
	}
	i++
	// string numeric suffix to uint64 bytes
	// empty string is zero, so integers are plus one
	b64 := make([]byte, 64/8)
	s64 := name[i:]
	if len(s64) > 0 {
		u64, err := strconv.ParseUint(s64, 10, 64)
		if err == nil {
			binary.BigEndian.PutUint64(b64, u64+1)
		}
	}
	// prefix + numeric-suffix + ext
	return name[:i] + string(b64) + ext
}

func main() {

	page := 1
	ocrsDir := "ocrs"
	screenshotsDir := "screenshots"

	// Get unsorted slice of screenshots
	_ = os.MkdirAll(ocrsDir, 0755)
	screenies, err := ioutil.ReadDir(screenshotsDir)
	if err != nil {
		log.Fatal(err)
	}

	// Sort screenies by number
	sort.Slice(
		screenies,
		func(i, j int) bool {
			return sortName(screenies[i].Name()) < sortName(screenies[j].Name())
		},
	)

	// Iterate and OCR each image
	page = 1
	for _, file := range screenies {
		screenie := fmt.Sprintf("%s/%s", screenshotsDir, file.Name())
		fmt.Printf("[*] Running OCR on pic: %s\n", screenie)
		ocrPath := fmt.Sprintf("%s/ocr_%d.txt", ocrsDir, page)
		ocr(screenie, ocrPath)
		page++
	}
}
