#!/bin/bash

# Install espeak to create MP3's using TTS
sudo apt install espeak

# Create virtual env for Python
if [[ ! -e env ]]; then
  python3 -m venv $(pwd)/env
fi

# Source virtual env
source env/bin/activate

# Ensure we have Mozilla TTS installed
if [[ ! -e "TTS-0.0.1+92aea2a-py3-none-any.whl" ]]; then
  wget "https://github.com/reuben/TTS/releases/download/ljspeech-fwd-attn-pwgan/TTS-0.0.1+92aea2a-py3-none-any.whl"
  pip3 install *.whl
fi

# Run TTS server in the background
python3 -m TTS.server.server --use_cuda 1 &

# Launch browser to TTS server page
xdg-open "http://localhost:5002"
